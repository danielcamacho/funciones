//ejercicios funciones
/*Si enumeramos todos los números naturales por debajo de 10 que son múltiplos de 3 o 5, obtenemos 3, 5, 6 y 9. La suma de estos múltiplos es 23.
Encuentra la suma de todos los múltiplos de 3 o 5 debajo de 1000.*/
/*var cont = 0;
var sum = 0;
while (cont <= 10) {
    cont++;
    if (!(cont % 3) || !(cont % 5)) {
        sum = sum + cont;
    }
}
//funcion factorial
var num = 5;
var fac = 1;
while (num > 0) {
    fac = fac * num;
    num--;
}
//funcion fibonacci
var fib1 = 0;
var fib2 = 1;
var fib3 = 1;
while (fib3 > 5) {
    console.log(fib3);
    fib3 = fib1 + fib2;
    fib1 = fib2;
    fib2 = fib3;
}*/
/*var cont = 0;
var sum = 0;
var num = 5;
var fac = 1;
var fib1 = 0;
var fib2 = 1;
var fib3 = 1;

function Multiplo_De_tres_O_Cinco() {
    while (cont <= 10) {
        cont++;
        if (!(cont % 3) || !(cont % 5)) {
            sum = sum + cont; //sum es la variable a llamar
        }
    }
}*/

function Factorial(n) {
    if ((n == 0) || (n == 1))
        return 1;
    else
        return (n * Factorial(n - 1));
}
console.log(Factorial(5));
/*function Serie_DE_Fibonacci() {
    while (fib3 <= 5) {
        console.log(fib3);
        fib3 = fib1 + fib2;
        fib1 = fib2;
        fib2 = fib3; //fib3 es la variable a llamar
    }
}
//para llamar la funcion se escribe nombre de la funcion();
//para imprimir la funcion se escribe console.log(variable);
//ejemplo:
Serie_DE_Fibonacci();
console.log(fib3);*/
/*function ciclo(a, b) {
    if (a != b) {
        while (a < b) {
            return a++;
        }
    } else {
        while (a > b) {
            return a--;
        }
    }
}
console.log(ciclo(1, 10));*/
function Multiplo(n) {
    if (!(n % 3) || !(n % 5))
        return (n * Multiplo(n + 1));
    else
        return 1;
}
console.log(Multiplo(10));